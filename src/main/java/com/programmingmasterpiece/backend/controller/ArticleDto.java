package com.programmingmasterpiece.backend.controller;

public class ArticleDto {

    public String description;

    public ArticleDto(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

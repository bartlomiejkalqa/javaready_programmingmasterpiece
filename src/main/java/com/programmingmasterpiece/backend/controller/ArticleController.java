package com.programmingmasterpiece.backend.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.programmingmasterpiece.backend.model.ArticleModel;
import com.programmingmasterpiece.backend.repository.ArticleRepository;
import com.programmingmasterpiece.backend.service.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/article")
public class ArticleController {

    final ArticleRepository articleRepository;
    final ArticleService articleService;

    Logger logger = LoggerFactory.getLogger(ArticleController.class);

    @Autowired
    public ArticleController(ArticleRepository articleRepository, ArticleService articleService) {
        this.articleRepository = articleRepository;
        this.articleService = articleService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @CrossOrigin(origins = {"https://localhost:3000", "http://programming-masterpiece.com", "https://programming-masterpiece.com"})
    public List<ArticleDto> article() {
        return articleService.findAllByOrderByCreatedDesc();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = {"https://localhost:3000", "http://programming-masterpiece.com", "https://programming-masterpiece.com"})
    public ArticleModel create(@RequestBody ArticleModel article) {
        article.setCreated(new Date());
        logger.info("logger2");
        return articleRepository.save(article);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    @CrossOrigin(origins = {"https://localhost:3000", "http://programming-masterpiece.com", "https://programming-masterpiece.com"})
    public Optional<ArticleModel> read(@PathVariable String id) {
        return articleRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/latest")
    @CrossOrigin(origins = {"https://localhost:3000", "http://programming-masterpiece.com", "https://programming-masterpiece.com"})
    public ArticleModel latest() {
        return articleRepository.findTopByOrderByCreatedDesc();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    @CrossOrigin(origins = {"https://localhost:3000", "http://programming-masterpiece.com", "https://programming-masterpiece.com"})
    public String delete(@PathVariable String id) {
        articleRepository.deleteById(id);
        return id;
    }
}
package com.programmingmasterpiece.backend.repository;

import java.util.List;

import com.programmingmasterpiece.backend.model.ArticleModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ArticleRepository extends MongoRepository<ArticleModel, String> {

    ArticleModel findTopByOrderByCreatedDesc();
    List<ArticleModel> findAllByOrderByCreatedDesc();
}

package com.programmingmasterpiece.backend.service;

import com.programmingmasterpiece.backend.controller.ArticleDto;
import com.programmingmasterpiece.backend.model.ArticleModel;
import org.springframework.stereotype.Component;

@Component
public class ArticleMapper {

    public ArticleDto map(ArticleModel articleModel) {
        return new ArticleDto(articleModel.getDescription());
    }
}

package com.programmingmasterpiece.backend.service;

import java.util.List;
import java.util.stream.Collectors;

import com.programmingmasterpiece.backend.controller.ArticleDto;
import com.programmingmasterpiece.backend.model.ArticleModel;
import com.programmingmasterpiece.backend.repository.ArticleRepository;
import org.springframework.stereotype.Component;

@Component
public class ArticleService {

    private final ArticleMapper articleMapper;
    private final ArticleRepository articleRepository;

    public ArticleService(ArticleMapper articleMapper, ArticleRepository articleRepository) {
        this.articleMapper = articleMapper;
        this.articleRepository = articleRepository;
    }

    public List<ArticleDto> findAllByOrderByCreatedDesc() {
        final List<ArticleModel> allByOrderByCreatedDesc = articleRepository.findAllByOrderByCreatedDesc();
        return allByOrderByCreatedDesc.stream()
                .map(articleMapper::map)
                .collect(Collectors.toList());
    }
}

package com.programmingmasterpiece.backend.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
public class ArticleParagraph {

    private String paragraphHeader;

    private List<ParagraphItem> paragraphContent;

    public ArticleParagraph(String paragraphHeader, List<ParagraphItem> paragraphContent) {
        this.paragraphHeader = paragraphHeader;
        this.paragraphContent = paragraphContent;
    }
}

package com.programmingmasterpiece.backend.model;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "articles")
@Getter
@Setter
public class ArticleModel {

    @Id
    private String id;

    private List<ArticleParagraph> content;

    private String header;

    private String description;

    private String category;

    private Date created;

    private String slug;

    public ArticleModel() {
    }

    public ArticleModel(String id, List<ArticleParagraph> content, String header, String description, String category, Date created, String slug) {
        this.id = id;
        this.content = content;
        this.header = header;
        this.description = description;
        this.category = category;
        this.created = created;
        this.slug = slug;
    }
}

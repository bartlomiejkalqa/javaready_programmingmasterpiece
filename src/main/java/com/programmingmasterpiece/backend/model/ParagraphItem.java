package com.programmingmasterpiece.backend.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
public class ParagraphItem {

    private String text;

    private String code;

    public ParagraphItem(String text, String code) {
        this.text = text;
        this.code = code;
    }
}
